/* Section */
const tabs = document.querySelector(".tabs");
const tabButton = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".content");

tabs.onclick = e => {
    const id = e.target.dataset.id;
    if (id) {
        tabButton.forEach(btn => {
            btn.classList.remove("active");
        });
        e.target.classList.add("active");

        contents.forEach(content => {
            content.classList.remove("active");
        });
        const element = document.getElementById(id);
        element.classList.add("active");
    }
}

/* Section filter */
const btnWork = document.querySelector('.learn-more');
let counter = 12;
const workImg = document.querySelectorAll(".work-image img");
const workTitles = document.querySelectorAll(".work li");
let filter = "all";
document.querySelector(".work").addEventListener("click", (event) => {
    if (event.target.innerText.length < 30) {
        selectCategoryTab(event.target);
        filter = event.target.dataset.filter;
        showWorksByCategory(event.target.dataset.filter);
    }
});
function selectCategoryTab(target) {
    for (const title of workTitles) {
        if (title === target) {
            title.classList.add("selected");
        }
        else {
            title.classList.remove("selected");
        }
    }
}
btnWork.addEventListener("click", function(event) {
        counter += 12;
        if (counter === 36) {
            btnWork.classList.add("hide");
        }
        setTimeout(() => {
            showWorksByCategory(filter);
        }, 2000);
    }
);
function showWorksByCategory(filterName) {
    if (filterName === "all") {
        for (let i = 0; i < counter; i++) {
            workImg[i].classList.remove("hide");
        }
        return;
    }
    for (let i = 0; i < counter; i++) {
        if (workImg[i].dataset.filter === filterName) {
            workImg[i].classList.remove("hide");
        }
        else {
            workImg[i].classList.add("hide");
        }
    }
}


/* Slick */
$(document).ready(function(){
    $('.slider').slick({
        arrows:true,
        dots:false,
        slidesToShow:(4),
        focusOnSelect: true,
        speed:3000,
        infinite: true,
        asNavFor:'.sliderbig',
        cssEasy: "linear",
    });
    $('.sliderbig').slick({
        arrows:false,
        fade:true,
        asNavFor:'.slider',
        cssEasy: "linear",
    })
});

/* Btn to top */
let button = $(".to-top");

$(window).scroll(function(){
    if ($(window).scrollTop() > 200 ){
        button.show()
    }else{
        button.hide()
    }
});

button.click(function(){
    $('html , body').animate({scrollTop:0},1000)
});

/* Cookies */
const btn = document.querySelector('.cookie-button');
const cookie = document.querySelector('.cookie-box');
let theme = localStorage.getItem('cookie');

window.onload = function () {
    if (localStorage.getItem('cookie') !== null) {
        cookie.classList.add("cookie-box-hide");
    }
}
btn.addEventListener('click', function(event) {
    localStorage.setItem('cookie', 'cookie');
    cookie.classList.add("cookie-box-hide");
})
